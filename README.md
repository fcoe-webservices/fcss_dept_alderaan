# FCSS Drupal 8 Department Theme - Layout Builder
This is the default Drupal 8 theme for FCOE Drupal departments and subsites. For sites using the original department theme, this site requires a migration to the new platform as well as the new theme. See the documentation below.

## Documentation ##

### [Upgrading to Layout Builder](docs/Upgrade.md) ###
* Migrating to the New Platform
* Deployment
* Post-Deployment Fixes
* Setting Up Site Content

### [Setting Up a Local Build](docs/Local-Development.md) ###
* Configuring Lando
* Configuring the Development Environment
* Configuring BrowserSync
* Compiling the Theme

### [Layout Builder Overview](docs/Layout-Builder.md) ###
* Configuration
* Default Layouts
* Restricting Access to Content
* Custom Blocks

### [Features Overview](docs/Features.md) ###
* Deploying Features
* Dealing with Site-Specific Configuration

### [Site-Specific Development](docs/Site-Development.md) ###
* Site Notes
* Site-Specific Feature Configuration

## Changelog ##

## January 20, 2020 ##
*************
### BugFixes ###
* __First:__ Note
* __Second:__ Note
* __Third:__ Note

### New Features ###
* __First:__ Note
* __Second:__ Note
* __Third:__ Note

### Performance Improvements
* __First:__ Note
* __Second:__ Note
* __Third:__ Note
