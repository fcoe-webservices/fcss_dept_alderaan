# Upgrading to Layout Builder #

### Migrating to the New Platform ###
*****
In Aegir, clone the site in the current platform ```fcss_dept_01.03``` and migrate it to ```fcss_dept_01.05```, then verify. Once the site is verified on the new platform, run the following drush commands. **Do not visit the site in the browser.**


### Deployment to Layout Builder ###
*****
```bash
drush en fcss_dept_deploy -y
drush updb -y
drush updb -y
```
**Note:** Run these commands before visiting the site, which can cause cache issues with Layout Builder.

The commands do the following:

1. Activates the fcss base deploy script, which reconfigures modules and features that cause conflicts with the migration to layout builder
2. Runs the config updates in the deploy module, including module installation and config, and activates the ```fcss_dept_deploy_lb``` module 
3. Runs the layout builder specific config on the site, while importing new features for all content types, blocks, views, etc.

### Post Deployment Fixes
*****
The following are a set of manual changes that are not possible via the deploy script.

#### Resetting The Block Layout
 
All nodes should use layout builder to place blocks, so several need to be disabled or moved to accommodate the new layouts. The skeleton of the block layout should appear as follows. If there are any styling errors, check that the machine readable names match those below.

**Header**

* Site Branding `fcss_dept_sitebranding`
* Main Navigation `fcss_dept_main_menu` **Do not use** `mainnavigation`

**Content**

* Help `fcss_dept_help`
* Tabs `tabs` **or** `fcss_dept_local_tasks`
* Main Page Content `fcss_dept_content`
* Page Title (disable) `fcss_dept_page_title`

All other blocks may be disabled or removed, based on the site. If there are any content types that have layout builder deactivated, restrict blocks to only appear on those content types. 

You may need to configure the `Main Navigation` menu block to only display the first level of the menu, which may be set to 'Unlimited'.

#### Manual Configuration Changes ####

1. The 'Quick Edit' module should be deactivated. **TODO:** Run this through HOOK_update

### Setting Up Site Content ###
*****
Both Deploy scripts should configure content types to display similar to the previous theme, without any customizing. There are a few exceptions

1.	**Recreate the Homepage:** Create a page titled 'Home' and customize the layout. Set the 'Home' page to be the default home page in Site Settings.

2. **Re-Place or Recreate Blocks within Layout Builder**

3. **Duplicate Views Pages into Views Blocks and place them on a Basic Page** Because of the styling and layout requirements of layout builder, it's easier to create a page and include a duplicate block than to have a views page, although this isn't required at all times.