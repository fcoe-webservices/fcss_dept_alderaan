# Features & Feature Configuration #

### Deploying Features ###
*****
Once a feature has been updated, it's deployed using the following command on the live site:
	
`drush @sites updb -y` or `drush updb` in a specific sites folder

Before the command can be run, the module and deploy script need to be changed on the live site. To do so, you'll need to

1. Export existing or create new feature locally
2. Update the local deploy script so that it imports the new feature
3. Push both changes to the repo and pull them down on the live site

### Exporting Feature Changes ###
*****
1. Visit the Features Overview page in the configuration section of the admin. 
2. Search for the feature that applies to the configuration changes, it should be marked as `Changed`.
3. Open the specific feature and either download the new feature (live site) or write to the feature module (locally)

### Creating New Features ###
*****
Features are organized according to their purpose, and are bundled in the `FCSS Department` Bundle. To create a new feature

1. Visit the Features Overview page in the configuration section of the admin. 
2. Click the Add Feature button at the top of the page
3. Features are blocked off by type and are named accordingly. In the feature page you'll see the feature types listed below.

* Block Type: xxxxxx (fcss_dept_bt__)
* Content Type: xxxxxx (fcss_dept_bt__)
* View: xxxxxx (fcss_dept_vt__)
* Vocabulary: xxxxxx (fcss_dept_tv__)
* Menu: xxxxxx (fcss_dept_menu__)
* Configuration Entity: xxxxxx (fcss_dept_ce__)

Once a new feature is created, the repo needs to commited then be pushed to bitbucket. Site-specific features should be stored in the `Sites` folder.

### Adding a Feature Change to the Deploy Script ###
*****
Updates to a feature can be pushed via the following steps:


1 - Add the following function to the end of the `fcss_dept_deploy_lb.install` in the FCSS Department Deploy (Layout Builder) module. Be sure to increment the module number incrementally (8711 to 8712) for each new deployment

```
/**
 * Feature Push: Fix ... on the ... Content Type/Block Type/etc.
 */

function fcss_dept_deploy_lb_update_87xx() {
  $module_list = array(
    // Replace or add to the line below with the names of each updated feature
    'fcss_dept_ct__page',  );
  $assigner = \Drupal::service('features_assigner');
  $assigner->assignConfigPackages();
  \Drupal::service('features.manager')->import($module_list);
}
``` 

### Pushing Features to the Repo ###
*****
All FCSS department specific custom modules are stored in the following repo

```https://bitbucket.org/fcoe-webservices/fcss-department-modules/src/master/```

instead of in individual repos.

Site-specific features are stored in the `site-specific` folder within the repo.
