# Site-Specific Development #

## Site Notes ##

Site notes are added via a `Site Notes` block type that is listed on the user page. The purpose of the notes are to document any site-specific changes that another admin may need to know. The block is simply a series of text fields with no limitation. Include the following.

* A title
* The date of the note
* A description of the change
* Where the configuration changes are located
* Any related theme/feature changes by commit

## Site-Specific Configuration ##

Sites with configuration that differs from the default can be handled in one of two ways

1. If the functionality doesn't provide a conflict with the existing configuration, add a note in the site notes, and optionally bundle the new configuration in a site-specific feature for local development. You can place the newly created feature in the `site-specific` folder within the modules repo.
2. If the functionality does provide a conflict, you may wish to deactivate the feature module that affects the configuration for that site. Add your new feature to the `site-specific folder` and import through the features interface, rather than the deploy script. Note the changes in the site notes.