# Setting up a Local Build

### Configuring Lando ###
*****
Use the following lando file for the initial configuration of a local platform. If you are using multisite, add addtional hosts and mysql commands to load multiples sites on a single multisite platform

```bash
name: SITE1
recipe: drupal8

config:
  webroot: .
  drush: ^8

proxy:
  appserver:
    - SITE2.lndo.site
    - SITE3.lndo.site

services:
  node:
    type: node
    overrides:
      ports:
      - 4000:4000
    globals:
      gulp-cli: latest

tooling:
  npm:
    service: node
  node:
    service: node
  gulp:
    service: node

```

Run `lando start` from the root to launch the project.

**Note:** There may be issues with the initial build where the additional databases (i.e. SITE2) is not created. If so, run the command ```lando mysql``` from the terminal, then enter the line above to create your additional databases.

### Setting up the site ###
To pull down the live environment, do the following

1. Download a clean version of Drupal that matches the live site.
<<<<<<< HEAD
2. Pull the `fcss_dept` theme and `modules/custom` folder from the live site and place them in the clean install
3. Pull the `sites/xxxxx` folder from the live site
4. Edit the `sites/xxxxx/settings.php` database info to reflect the lando db configuration
5. Edit the `sites/sites.php` file to match the lando URL configuration
6. For multiple sites, each new database needs to be added manually. To so, run ```lando mysql``` Using the following command. 
=======
2. Pull the `fcss_dept` theme and `modules/custom` folder from their repos and place them in the clean install
3. Pull the `sites/xxxxx` folder from the live site
4. Edit the `sites/xxxxx/settings.php` database info to reflect the lando db configuration
5. Edit the `sites/sites.php` file to match the lando URL configuration (sitename.lndo.site => sitename.fcoe.org)
6. For each new site, add their database by running ```lando mysql``` Using the following command: 
>>>>>>> 9466a3cf10d4234b0fdf1891598d6e9fadd8efcf

```
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS SITE2; GRANT ALL PRIVILEGES ON
SITE2.* TO 'drupal8'@'%' IDENTIFIED by 'drupal8';"
```
<<<<<<< HEAD

Once the database is created, you can import it into the site using Sequel Pro.
=======
**Note:** be sure to change the word `SITE2` in the command above to reflect the database name for the specific site.

After the DB is created, import the DB from the live site via Sequel Pro.
>>>>>>> 9466a3cf10d4234b0fdf1891598d6e9fadd8efcf

### Configuring the Development Environment ###
*****

#### Activate the settings.local.php file ####
Copy the ```example.settings.local.php``` file to the root of your ```sites/SITENAME/``` and rename ```settings.local.php```. Be sure that the settings.php file has the reference to the local file uncommented.

#### Disable Caching and Activate Twig Debugging ####
Use the [following documentation](https://www.drupal.org/node/2598914) to set up the development environment.

After disabling the cache, run `lando drush cr` in the `/sites/SITENAME/` folder root to activate the changes.

**Note:** If you are experiencing issues with error warnings that won't disappear, be sure to comment out the following line in the `settings.local.php` file.

```
$config['system.logging']['error_level'] = 'verbose';
```

### BrowserSync Configuration ###
*****
change the proxy address within the `gulpfile.js` file at the root of the theme to match your lando address.

```
    browserSync.init({
      ui: {
        port: 4001
      },
      port: 4000,
      proxy: "https://SITE1.lndo.site"
    });
```

### Compile the Theme ###
*****
Install the required node modules to run gulp, then start gulp

```
lando npm install
lando gulp
```

changes to SCSS files will now compile the css automatically.
