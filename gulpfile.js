'use strict';

// Load Gulp and tools we will use.
var $               = require('gulp-load-plugins')(),
    browserSync     = require('browser-sync'),
    del             = require('del'),
    extend          = require('extend'),
    fs              = require("fs"),
    gulp            = require('gulp'),
    importOnce      = require('node-sass-import-once');

var options = {};

options.gulpWatchOptions = {};

// The root paths are used to construct all the other paths in this
// configuration. The "project" root path is where this gulpfile.js is located.
// While ZURB Foundation distributes this in the theme root folder, you can also
// put this (and the package.json) in your project's root folder and edit the
// paths accordingly.
options.rootPath = {
  project     : __dirname + '/',
  theme       : __dirname + '/'
};

options.theme = {
  root       : options.rootPath.theme,
  scss       : options.rootPath.theme + 'sass/',
  css        : options.rootPath.theme + 'css/'
};

// Define the node-scss configuration.
options.scss = {
  importer: importOnce,
  outputStyle: 'compressed',
  lintIgnore: ['sass/_settings.scss', 'sass/base/_drupal.scss'],
  includePaths: [
    options.rootPath.project + 'node_modules/foundation-sites/scss',
    options.rootPath.project + 'node_modules/motion-ui/src'
  ],
};

// Define which browsers to add vendor prefixes for.
options.autoprefixer = {
  browsers: [
    'last 2 versions',
    'ie >= 9'
  ]
};

// If config.js exists, load that config and overriding the options object.
if (fs.existsSync(options.rootPath.project + "/config.js")) {
  var config = {};
  config = require("./config");
  extend(true, options, config);
}

var scssFiles = [
  options.theme.scss + '**/*.scss',
  // Do not open scss partials as they will be included as needed.
  '!' + options.theme.scss + '**/_*.scss',
];


// BrowserSync
gulp.task('browser-sync', function() {
    browserSync.init({
      ui: {
        port: 4001
      },
      port: 4000,
      proxy: "https://stem-lb.lndo.site"
    });
});

// The default task.
gulp.task('default', ['build', 'browser-sync']);

// Build everything.
gulp.task('build', ['sass']);

// Default watch task.
// @todo needs to add a javascript watch task.
gulp.task('watch', ['watch:css', 'watch:html', 'browser-sync']);

// Watch for changes for scss files and rebuild.
gulp.task('watch:css', ['sass'], function () {
  return gulp.watch(["sass/**/*.scss"], options.gulpWatchOptions, ['sass']);

});


gulp.task('watch:html', function () {
  return gulp.watch("templates/**/*.twig").on('change', browserSync.reload);
});

// Lint Sass and JavaScript.
// @todo needs to add a javascript lint task.
gulp.task('lint', ['lint:sass']);

// Build CSS for development environment.
gulp.task('sass', ['clean:css'], function () {
  return gulp.src(scssFiles)
    .pipe($.sourcemaps.init())
    // Allow the options object to override the defaults for the task.
    .pipe($.sass(extend(true, {
      noCache: true,
      outputStyle: options.scss.outputStyle,
      sourceMap: true
    }, options.scss)).on('error', $.sass.logError))
    .pipe($.autoprefixer(options.autoprefixer))
    .pipe($.rename({dirname: ''}))
    .pipe($.size({showFiles: true}))
    .pipe($.sourcemaps.write('./'))
    .pipe($.concat('app.css'))
    .pipe(gulp.dest(options.theme.css))
    .pipe(browserSync.stream());
});

// Clean CSS files.
gulp.task('clean:css', function () {
//  return del([
//    options.theme.css + '**/*.css',
//    options.theme.css + '**/*.map'
//  ], {force: true});
});

// Defines a task that triggers a Drush cache clear (css-js), you need to edit
// config.js to be able to use this task.
gulp.task('drush:cc', function () {
  if (!options.drush.enabled) {
    return;
  }

  return gulp.src('', {read: false})
    .pipe($.shell([
      options.drush.alias.css_js
    ]));
});

// Lint Sass.
gulp.task('lint:sass', function () {
  return gulp.src(options.theme.scss + '**/*.scss')
    // use gulp-cached to check only modified files.
    .pipe($.sassLint({
      files: {
        include: $.cached('scsslint'),
        ignore: options.scss.lintIgnore
      }
    }))
    .pipe($.sassLint.format());
});
